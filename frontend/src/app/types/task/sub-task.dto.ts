export class SubTaskDto {
  id!: number;
  taskId!: number;
  description!: string;
}