import { TaskStatusEnum } from "./task-status.enum";
import { CategoryDto } from "./category.dto";
import { SubTaskDto } from "./sub-task.dto";

export class TaskDto {
  id!: number;
  description!: string;
  deadline!: Date;
  created_at!: Date;
  status!: TaskStatusEnum;
  sub_tasks!: SubTaskDto[];
  categories!: CategoryDto[];
}