export class UserModel {
	id?: number;
	name?: string;
	email?: string;
	token?: string;
	created_at?: Date;
	updated_at?: Date;
}
