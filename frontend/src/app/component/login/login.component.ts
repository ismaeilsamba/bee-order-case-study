import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { BASE_URL } from 'src/config/config';
import { UserModel } from 'src/app/types';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent {
	errors: {
		message: any;
	} = {
		message: null
	};

	constructor(private httpClient: HttpClient, private router: Router, private location: Location) {}

	ngOnInit(): void {}

	onSubmit(form: NgForm) {
		const email = form.value.email;
		const password = form.value.password;

		this.httpClient
			.post<UserModel>(`${BASE_URL}/login`, {
				email: email,
				password: password
			})
			.subscribe({
				next: (res: UserModel) => {
					const userJson = JSON.stringify(res);
					localStorage.setItem('user', userJson);

					this.router.navigate(['/']).then(() => {
						this.location.replaceState('/');
						window.location.reload();
					});
				},
				error: (err) => {
					this.errors.message = err.error.message;
					console.error({ err });
				}
			});
	}
}
