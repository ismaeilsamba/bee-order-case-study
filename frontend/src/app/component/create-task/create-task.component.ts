import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { BASE_URL } from 'src/config/config';
import { UserModel } from 'src/app/types';

@Component({
	selector: 'app-create-task',
	templateUrl: './create-task.component.html',
	styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent {
	selectedCategories: string[] = [];
	selectedSubTasks: string[] = [];
	categories: string[] = [];

	constructor(private httpClient: HttpClient, private router: Router) {}

	ngOnInit(): void {
		const userJson = localStorage.getItem('user');
		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}
		const user = JSON.parse(userJson).user as UserModel;

		this.httpClient
			.get(`${BASE_URL}/category`, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${user.token}`
				})
			})
			.subscribe({
				next: (res: any) => {
					this.categories = [];
					for (let i of res) {
						this.categories.push(i.name);
					}
				}
			});
	}

	addNewSubTask(form: NgForm) {
		const newSubTask = form.value.addSubTask;
		if (newSubTask.trim() !== '') {
			this.selectedSubTasks.push(newSubTask);
		}
	}

	addNewCategory(form: NgForm) {
		const newCategory = form.value.addCategory;
		if (newCategory.trim() !== '') {
			this.selectedCategories.push(newCategory);
		}
	}

	onSubmit(form: NgForm) {
		const userJson = localStorage.getItem('user');

		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}

		const user: UserModel = JSON.parse(userJson).user as UserModel;

		const description = form.value.description;
		const date = new Date(form.value.deadline);
		const deadline = this.formatDateToYYYYMMDDHHMMSS(date);
		const categories = this.selectedCategories.map((c) => {
			return {
				name: c
			};
		});
		const subTasks = this.selectedSubTasks.map((s) => {
			return {
				description: s
			};
		});

		this.httpClient
			.post(
				`${BASE_URL}/task`,
				{
					description: description,
					deadline: deadline,
					subTask: subTasks,
					category: categories
				},
				{
					headers: new HttpHeaders({
						Authorization: `Bearer ${user.token}`
					})
				}
			)
			.subscribe({
				next: () => {
					this.router.navigate(['/']);
				},
				error: (err) => console.error(err)
			});
	}

	formatDateToYYYYMMDDHHMMSS(date: Date) {
		const year = date.getFullYear();
		const month = String(date.getMonth() + 1).padStart(2, '0'); // Adding 1 because months are zero-based
		const day = String(date.getDate()).padStart(2, '0');
		const hours = String(date.getHours()).padStart(2, '0');
		const minutes = String(date.getMinutes()).padStart(2, '0');
		const seconds = String(date.getSeconds()).padStart(2, '0');

		return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
	}
}
