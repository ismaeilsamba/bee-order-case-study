import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { BASE_URL } from 'src/config/config';
import { UserModel } from 'src/app/types';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent {
	errors: {
		name: any;
		email: any;
		password: any;
	} = {
		name: null,
		email: null,
		password: null
	};

	constructor(private httpClient: HttpClient, private router: Router) {}

	ngOnInit(): void {}

	onSubmit(form: NgForm) {
		const name = form.value.name;
		const email = form.value.email;
		const password = form.value.password;
		const password_confirmation = form.value.password_confirmation;

		if (password !== password_confirmation) {
			this.errors.password = 'password_confirmation and password are not equal!';
		}

		this.httpClient
			.post<UserModel>(`${BASE_URL}/register`, {
				name: name,
				email: email,
				password: password
			})
			.subscribe({
				next: () => {
					this.router.navigate(['login']);
				},
				error: (err) => {
					this.errors.name = err.error.message.name?.length > 0 ? err.error.message.name[0] : null;
					this.errors.email = err.error.message.email?.length > 0 ? err.error.message.email[0] : null;
					this.errors.password = err.error.message.password?.length > 0 ? err.error.message.password[0] : null;
				}
			});
	}
}
