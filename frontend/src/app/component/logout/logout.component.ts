import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BASE_URL } from 'src/config/config';
import { UserModel } from 'src/app/types';

@Component({
	selector: 'app-logout',
	templateUrl: './logout.component.html',
	styleUrls: ['./logout.component.css']
})
export class LogoutComponent {
	constructor(private httpClient: HttpClient, private router: Router, private location: Location) {}

	ngOnInit(): void {
		const userJson = localStorage.getItem('user');
		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}
	}

	logout() {
		const userJson = localStorage.getItem('user');

		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}

		const user: UserModel = JSON.parse(userJson).user as UserModel;
		localStorage.removeItem('user');

		this.httpClient
			.post(
				`${BASE_URL}/logout`,
				{},
				{
					headers: new HttpHeaders({
						Authorization: `Bearer ${user.token}`
					})
				}
			)
			.subscribe({
				complete: () => {
					this.router.navigate(['login']).then(() => {
						this.location.replaceState('login');
						window.location.reload();
					});
				}
			});
	}
}
