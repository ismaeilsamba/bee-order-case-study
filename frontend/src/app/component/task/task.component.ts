import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TaskDto, UserModel } from 'src/app/types';
import { BASE_URL } from 'src/config/config';

@Component({
	selector: 'app-task',
	templateUrl: './task.component.html',
	styleUrls: ['./task.component.css']
})
export class TaskComponent {
	currentPage: number = 1;
	tasks: TaskDto[] = [];
	limit: number = 5;
	q: string = '';

	constructor(private httpClient: HttpClient, private router: Router) {}

	ngOnInit(): void {
		this.getTasks();
	}

	getTasks() {
		const userJson = localStorage.getItem('user');
		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}
		const user = JSON.parse(userJson).user as UserModel;

		this.httpClient
			.get(`${BASE_URL}/task?page=${this.currentPage}&limit=${this.limit}&q=${this.q}`, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${user.token}`
				})
			})
			.subscribe({
				next: (res: any) => {
					this.tasks = [];
					for (let i of res.data) {
						this.tasks.push(i);
					}
				}
			});
	}

	onSubmit(form: any) {
		if (form === 'lastPage') {
			this.currentPage = Math.max(1, this.currentPage - 1);
		} else if (form === 'nextPage') {
			if (this.tasks.length === this.limit) {
				this.currentPage = this.currentPage + 1;
			}
		} else {
			this.q = form.value.search;
		}

		this.ngOnInit();
	}
}
