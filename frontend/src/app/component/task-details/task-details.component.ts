import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';

import { TaskDto, UserModel } from 'src/app/types';
import { BASE_URL } from 'src/config/config';

@Component({
	selector: 'app-task-details',
	templateUrl: './task-details.component.html',
	styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent {
	id!: string;
	task!: TaskDto;

	constructor(private route: ActivatedRoute, private router: Router, private httpClient: HttpClient) {}

	ngOnInit(): void {
		const userJson = localStorage.getItem('user');
		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}
		const user = JSON.parse(userJson).user as UserModel;

		this.id = this.route.snapshot.paramMap.get('id') || '0';

		this.httpClient
			.get<TaskDto>(`${BASE_URL}/task/${this.id}`, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${user.token}`
				})
			})
			.subscribe({
				next: (res: TaskDto) => {
					this.task = res;
					console.log({ res });
				}
			});
	}
}
