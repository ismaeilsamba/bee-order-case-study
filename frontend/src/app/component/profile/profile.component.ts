import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from 'src/app/types';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	constructor(private router: Router) {}

	user?: UserModel;

	ngOnInit(): void {
		const userJson = localStorage.getItem('user');
		if (!userJson) {
			this.router.navigate(['login']);
			return;
		}
		this.user = JSON.parse(userJson).user;
	}
}
