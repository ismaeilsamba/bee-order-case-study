import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { TaskDetailsComponent } from './component/task-details/task-details.component';
import { AuthNavbarComponent } from './component/auth-navbar/auth-navbar.component';
import { CreateTaskComponent } from './component/create-task/create-task.component';
import { RegisterComponent } from './component/register/register.component';
import { ProfileComponent } from './component/profile/profile.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { LogoutComponent } from './component/logout/logout.component';
import { LoginComponent } from './component/login/login.component';
import { TaskComponent } from './component/task/task.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent,
		TaskComponent,
		LoginComponent,
		LogoutComponent,
		NavbarComponent,
		ProfileComponent,
		RegisterComponent,
		AuthNavbarComponent,
		CreateTaskComponent,
		TaskDetailsComponent
	],
	imports: [FormsModule, BrowserModule, AppRoutingModule, HttpClientModule],
	bootstrap: [AppComponent]
})
export class AppModule {}
