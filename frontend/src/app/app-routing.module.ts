import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CreateTaskComponent } from './component/create-task/create-task.component';
import { RegisterComponent } from './component/register/register.component';
import { ProfileComponent } from './component/profile/profile.component';
import { LogoutComponent } from './component/logout/logout.component';
import { LoginComponent } from './component/login/login.component';
import { TaskComponent } from './component/task/task.component';
import { TaskDetailsComponent } from './component/task-details/task-details.component';

const routes: Routes = [
	{
		path: '',
		component: TaskComponent
	},
	{
		path: 'create-task',
		component: CreateTaskComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'logout',
		component: LogoutComponent
	},
	{
		path: 'register',
		component: RegisterComponent
	},
	{
		path: 'profile',
		component: ProfileComponent
	},
	{
		path: 'task-details/:id',
		component: TaskDetailsComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
