# Simple document shows the needed APIs in the system (Written before coding)

## Category

- route: `GET /api/category`
	- description
		- api to get all stored categories

## User

- route: `POST /api/user/login`
	- description
		- api to login user and authenticate him
	- body
		```php
		{
			email: string | email | length(1 ,128);
			password: string | length(8 ,64);
		}
		```

- route: `POST /api/user/register`
	- description
		- api to add new user to the system
	- body
		```php
		{
			name: string | length(1 ,128);
			email: string | email | length(1 ,128);
			password: string | length(8 ,64);
		}
		```

- route: `GET /api/user/me`
	- description
		- api to get the current authenticated user

## Task

- route: `POST /api/task`
	- description
		- api to create a new task with its subtasks and link it with at least one category
	- body
		```php
		{
			description: string | length(1 ,1024);
			deadline: date;
			subTask: [{
				description: string | length(1 ,1024);
			}];
			category: [{
				name: string | length(1 ,64);
			}];
		}
		```

- route: `GET /api/task`
	- description
		- api to get all tasks (without details)

- route: `GET /api/task/:id`
	- description
		- api to get one task (with all its details)
