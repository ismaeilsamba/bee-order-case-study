<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run(): void
    {
        $categories = [
            [
                'name' => 'backend',
                'color' => '#F2104C',
            ],
            [
                'name' => 'frontend',
                'color' => '#952323',
            ],
            [
                'name' => 'database',
                'color' => '#EC53B0',
            ],
            [
                'name' => 'state management',
                'color' => '#C08261',
            ],
            [
                'name' => 'api',
                'color' => '#435334',
            ],
            [
                'name' => 'ui',
                'color' => '#F6635C',
            ],
        ];

        foreach ($categories as $category) {
            DB::table('categories')->insert($category);
        }
    }
}
