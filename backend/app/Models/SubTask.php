<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubTask extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'description',
        'taskId'
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
