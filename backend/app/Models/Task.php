<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'deadline'
    ];

    protected $hidden = ['timestamp'];

    public function sub_tasks()
    {
        return $this->hasMany(SubTask::class, 'taskId', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'task_categories', 'taskId', 'categoryId');
    }
}
