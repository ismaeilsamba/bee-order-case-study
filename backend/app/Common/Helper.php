<?php

namespace App\Common;

class Helper
{
  static public function dateCompare(\DateTime $date1 ,\DateTime $date2) {
    $year1 = (int)$date1 -> format('Y');
    $year2 = (int)$date2 -> format('Y');
    if( $year1 > $year2 ) return +1;
    if( $year1 < $year2 ) return -1;

    $month1 = (int)$date1 -> format('m');
    $month2 = (int)$date2 -> format('m');
    if( $month1 > $month2 ) return +1;
    if( $month1 < $month2 ) return -1;

    $day1 = (int)$date1 -> format('d');
    $day2 = (int)$date2 -> format('d');
    if( $day1 > $day2 ) return +1;
    if( $day1 < $day2 ) return -1;

    $hour1 = (int)$date1 -> format('H');
    $hour2 = (int)$date2 -> format('H');
    if( $hour1 > $hour2 ) return +1;
    if( $hour1 < $hour2 ) return -1;

    $minute1 = (int)$date1 -> format('i');
    $minute2 = (int)$date2 -> format('i');
    if( $minute1 > $minute2 ) return +1;
    if( $minute1 < $minute2 ) return -1;

    $second1 = (int)$date1 -> format('s');
    $second2 = (int)$date2 -> format('s');
    if( $second1 > $second2 ) return +1;
    if( $second1 < $second2 ) return -1;

    return 0;
  }
}
