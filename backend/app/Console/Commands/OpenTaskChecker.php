<?php

namespace App\Console\Commands;

use App\Common\Helper;
use Illuminate\Console\Command;
use App\Models\Task;

class OpenTaskChecker extends Command
{
    protected $signature = 'app:open-task-checker';
    protected $description = 'Update the status of tasks to end_task when the deadline end';

    public function handle()
    {
        $date1 = new \DateTime("now", new \DateTimeZone("Etc/GMT-3"));
        $tasks = Task::where('status', '=', 'open_task')->get();

        foreach($tasks as $task){
            $date2 = new \DateTime($task -> deadline);
            if( Helper::dateCompare($date1 ,$date2) >= 0 ){
                $currentTask = Task::find($task -> id);
                $currentTask -> status = 'end_task';
                $currentTask -> save();
            }
        }
    }
}
