<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\TaskCategory;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubTask;
use App\Common\Helper;
use App\Models\Task;
use Exception;

class TaskController extends Controller
{
    public function getTask(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'limit' => 'nullable|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        // read query params
        $q = $req->query('q') ? (string)$req->query('q') : "";
        $limit = $req->query('limit') ? (int)$req->query('limit') : 10;

        // query tasks with search and pagination
        $task = Task::where('description', 'LIKE', '%' . $q . '%')->simplePaginate($limit);
        return response()->json($task, 200);
    }

    public function getTaskById($id)
    {
        // get the task with all sub_tasks and categories related to it
        $task = Task::with('sub_tasks', 'categories')->find($id);

        if (is_null($task)) {
            return response()->json(['message' => 'Task not found!'], 404);
        }

        return response()->json($task, 200);
    }

    public function createTask(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'description' => 'string|required|min:1|max:1024',
            'deadline' => [
                'required',
                'regex:/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/',
            ],
            'subTask' => 'array',
            'subTask.*.description' => 'string|required|min:1|max:1024',
            'category' => 'array',
            'category.*.name' => 'string|required|min:1|max:64',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        // create transaction to insert the task and its subTasks, categories to the database
        DB::beginTransaction();

        try {
            // insert task into database
            $task = new Task;
            $task->fill([
                'description' => $req->input('description'),
                'deadline' => $req->input('deadline')
            ]);
            $task->save();

            // insert subtask into database
            foreach ($req->input('subTask') as $subTaskInput) {
                $subTask = new SubTask;
                $subTask->fill([
                    'description' => $subTaskInput['description'],
                    'taskId' => $task['id']
                ]);
                $subTask->save();
            }

            // insert task-category into database
            foreach ($req->input('category') as $categoryInput) {
                $category = Category::where('name', $categoryInput['name'])->first();
                if (is_null($category)) {
                    throw new Exception($categoryInput['name'] . ' category is not found!');
                }

                $taskCateogry = new TaskCategory;
                $taskCateogry->fill([
                    'taskId' => $task['id'],
                    'categoryId' => $category['id']
                ]);
                $taskCateogry->save();
            }

            DB::commit();
        } catch (Exception $err) {
            DB::rollback();
            return response()->json(['message' => $err->getMessage()], 400);
        }

        return $this->getTaskById($task->id);
    }
}
