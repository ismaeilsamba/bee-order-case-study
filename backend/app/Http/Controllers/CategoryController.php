<?php

namespace App\Http\Controllers;

use App\Models\Category as ModelsCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getCategory()
    {
        return response()->json(ModelsCategory::all(), 200);
    }
}
