<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
  Route::post('login', 'login');
  Route::post('register', 'register');
  Route::post('logout', 'logout');
  Route::get('me', 'getMe');
});

Route::get('/category', [CategoryController::class, 'getCategory'])->middleware(['auth:api']);

Route::get('/task', [TaskController::class, 'getTask'])->middleware(['auth:api']);

Route::get('/task/{id}', [TaskController::class, 'getTaskByID'])->middleware(['auth:api']);

Route::post('/task', [TaskController::class, 'createTask'])->middleware(['auth:api']);
